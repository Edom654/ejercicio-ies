import { Component, OnInit } from '@angular/core';
import { WebsocketService } from '../../services/websocket.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']

})
export class LoginComponent implements OnInit {

  login = {
    email: '',
    password: ''
  };

  constructor( private ws: WebsocketService, private router: Router ) { }
  // url = 'https://ies-webcontent.com.mx/xccm/user/validarUsuario';
  url = 'wss://echo.websocket.org';

  ngOnInit() {
this.ws.connect(this.url).subscribe(data => {
  console.log('Mostrando servicio', data);
});


  }

  loginOn() {
    if (this.login.email === 'dist03' && this.login.password === 'dist03') {
      console.log('Bienvenido Distribuidor');
      this.router.navigate(['dashboard']);
    } else {
      console.log('Error', Error);
    }
  }

}
