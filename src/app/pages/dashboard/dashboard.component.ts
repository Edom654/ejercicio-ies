import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../../services/service.service';

let Swal: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  dia: any = [] = [];
  city: any = [];
  OB: any = [];

  prueba: any = [];

  form = {
    email: '',
    nombre: '',
    fecha: '',
    pais: ''
  };

  constructor( public service: ServiceService ) { }

  ngOnInit() {
    this.getFecha();
    this.getService();
    var fecha = new Date();
    console.log('Fecha', fecha);
  }

  getFecha() {
    const fecha = new Date();
      // console.log(fecha);

      const dia = {
        dd: fecha.getDate() + 1,
        mm: fecha.getMonth() + 1,
        yy: fecha.getFullYear()
    };
    this.dia =  dia;
    // console.log('Mostrando', this.dia, this.form.fecha);
  }

  getService() {
    let prueba: any = [];
    this.service.getAPI().subscribe((res: any) => {
        // console.log(res);
        this.city = this.city.concat(res.data);
        // console.log('CITY', this.city);
        this.city.map(resp => {
          prueba = resp;
        // this.OB = this.OB.concat(resp.DZ);
        // console.log('CONCAT', prueba.AO, prueba.DZ);
        this.OB.push(prueba.DZ, prueba.AO, prueba.BJ, prueba.BW, prueba.BF, prueba.BI, prueba.CV, prueba.CM, prueba.CF, prueba.TD);
        console.log('FInal', this.OB);
        });
    });
  }

  sendForm() {
    // if(this.dia >= this.form.fecha) {
      // console.log('PASA');
    // } else {
      // console.log('Error');
    // }
    if ( this.form.nombre !== '' && this.form.fecha !== '' && this.form.email !== '') {
  
      console.log('Mostrando FORMS', this.form);
    } else {
      console.log('Error', Error);
    }
  }

}
