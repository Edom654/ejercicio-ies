import { Component, OnInit } from '@angular/core';
import { connectableObservableDescriptor } from 'rxjs/observable/ConnectableObservable';

@Component({
  selector: 'app-componete-uno',
  templateUrl: './componete-uno.component.html',
  styleUrls: ['./componete-uno.component.css']
})
export class ComponeteUnoComponent implements OnInit {

  // tslint:disable-next-line:max-line-length
  array1 = [{ value: 1, name: 'CampoUno' }, { value: 2, name: 'CampoDos' }, { value: 3, name: 'CampoTres' }, { value: 4, name: 'CampoCuatro' }, { value: 5, name: 'CampoCinco' }, { value: 6, name: 'CampoSeis' }];

  // tslint:disable-next-line:max-line-length
  array2 = [{ value: 21, name: 'a' }, { value: 20, name: 'b' }, { value: 19, name: 'c' }, { value: 18, name: 'd' }, { value: 17, name: 'e' }, { value: 16, name: 'f' }];

  obj1 = { 'CampoUno': 1, 'CampoDos' : 2, 'CampoTres': 3, 'CampoCuatro': 4, 'CampoCinco': 5, 'CampoSeis': 6 };

  arrayA: any = [];
  arrayB: any = {};

  Obj = [];

  constructor() { }

  ngOnInit() {
    this.getArray1();
    this.getArray2();
    this.getObj();
    console.log('ARRAY 1', this.array1);
  }

  getArray1() {
    // this.array1.forEach(res => {
      // this.arrayA = this.arrayA.concat(res.name + ':' + res.value);
      // console.log('Mostrando ARRAY #1', this.arrayA);
    // });

    this.arrayA = Object.assign( {}, this.array1 );
    console.log('Mostrando Array #1', this.arrayA);
  }

  getArray2() {
    let prueba = [];
    prueba = Object.assign({}, this.array2);
    this.arrayB = prueba;
    console.log('Mostrando Array #2', this.arrayB);
  }

  getObj() {
    let value: any = [];
    let name: any = [];
    // this.Obj = Object.assign(this.obj1);
  //  this.Obj = this.Obj.concat(this.obj1);
  // value = Object.values(this.obj1);
  // name = Object.keys( this.obj1);

  // tslint:disable-next-line:no-unused-expression
  // this.Obj = this.Obj.concat(value + name);
    // console.log('Mostrando OBJ -> ARRAY', this.Obj);
    this.Obj = Object.entries(this.obj1);
    console.log('Prueba', this.Obj);
  }

}
