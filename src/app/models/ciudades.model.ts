export interface listaCiudades {
    status: string;
    version: string;
    total: string;
    limit: string;
    offset: string;
    access: string;
    data: {
        DZ: string;
        AO: string;
        BJ: string;
        BW: string;
        BF: string;
        BI: string;
        CV: string;
        CM: string;
        CF: string;
        TD: string;
    };
}
