import { Injectable } from '@angular/core';
// import { HttpClientModule } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { HttpClient } from '@angular/common/http';

import {listaCiudades} from '../models/ciudades.model';
import { Observable } from 'rxjs/Observable';

@Injectable()

export class ServiceService {

    API = [];

    url: string = 'https://api.first.org/data/v1/countries?region=africa&limit=10&pretty=true';
    constructor( private http: HttpClient ) {}

    getAPI():Observable<listaCiudades[]> {
        const direccion = this.url;
        return this.http.get<listaCiudades[]>(direccion);
    }
}
